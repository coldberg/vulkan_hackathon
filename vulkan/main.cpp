#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <tuple>
#include <fstream>
#include <experimental/filesystem>

#include "vkfw_handle.hpp"
#include "vkfw_instance.hpp"
#include "vkfw_exception.hpp"
#include "vkfw_proc_addr.hpp"
#include "vkfw_debug_report.hpp"
#include "vkfw_surface.hpp"

#define GLFW_INCLUDE_VULKAN
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include <GLFW/glfw3.h>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

namespace std
{
	using namespace experimental;
}


struct GlideApp 
{
	void setup_window () {
		glfwInit ();
		glfwWindowHint (GLFW_CLIENT_API, GLFW_NO_API);
		m_window = glfwCreateWindow (800, 600, "Vulkan window", nullptr, nullptr);
	}

	void teardown_window () {
		if (m_window != nullptr) 
		{
			glfwDestroyWindow (m_window);
		}
		glfwTerminate ();
	}

	static auto verify_vkresult (VkResult loc_result)
	{
		vkfw::maybe_throw (loc_result);
		if (loc_result != VK_SUCCESS)
		{
			std::cout << vkfw::exception (loc_result).what() << "\n";
		}
	}

	static auto surface_capabilities(const VkPhysicalDevice& loc_device, const VkSurfaceKHR& loc_surface)
	{
		VkSurfaceCapabilitiesKHR loc_caps;
		auto loc_result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(loc_device, loc_surface, &loc_caps);
		verify_vkresult(loc_result);

		std::uint32_t loc_count = 0u;

		loc_result = vkGetPhysicalDeviceSurfaceFormatsKHR(loc_device, loc_surface, &loc_count, nullptr);
		verify_vkresult(loc_result);
		std::vector<VkSurfaceFormatKHR> loc_formats(loc_count);
		loc_result = vkGetPhysicalDeviceSurfaceFormatsKHR(loc_device, loc_surface, &loc_count, loc_formats.data());
		verify_vkresult(loc_result);

		loc_result = vkGetPhysicalDeviceSurfacePresentModesKHR(loc_device, loc_surface, &loc_count, nullptr);
		verify_vkresult(loc_result);
		std::vector<VkPresentModeKHR> loc_modes(loc_count);
		loc_result = vkGetPhysicalDeviceSurfacePresentModesKHR(loc_device, loc_surface, &loc_count, loc_modes.data());
		verify_vkresult(loc_result);

		return std::make_tuple(loc_caps, loc_formats, loc_modes);
	}

	const VkAllocationCallbacks* allocators () const
	{
		return nullptr;
	}

	static auto load_binary (const std::string& loc_path)
	{
		std::vector<char> loc_data (std::filesystem::file_size(loc_path));
		std::ifstream loc_stream (loc_path, std::ios::binary);
		loc_stream.read (loc_data.data(), loc_data.size ());
		return loc_data;
	}

	void setup_logical_device () 
	{
		const auto loc_queue_index = m_vkfwQueueIndex;
		const auto loc_one = 1.0f;
		VkDeviceQueueCreateInfo loc_queue_info =
		{
			VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO, 
			nullptr,
			0u,
			loc_queue_index,
			1u,
			&loc_one
		};
		
		VkPhysicalDeviceFeatures loc_features =
		{
			VK_FALSE,//VkBool32    robustBufferAccess;
			VK_FALSE,//VkBool32    fullDrawIndexUint32;
			VK_FALSE,//VkBool32    imageCubeArray;
			VK_FALSE,//VkBool32    independentBlend;
			VK_TRUE, //VkBool32    geometryShader;
			VK_TRUE, //VkBool32    tessellationShader;
			VK_FALSE,//VkBool32    sampleRateShading;
			VK_FALSE,//VkBool32    dualSrcBlend;
			VK_FALSE,//VkBool32    logicOp;
			VK_FALSE,//VkBool32    multiDrawIndirect;
			VK_FALSE,//VkBool32    drawIndirectFirstInstance;
			VK_FALSE,//VkBool32    depthClamp;
			VK_FALSE,//VkBool32    depthBiasClamp;
			VK_FALSE,//VkBool32    fillModeNonSolid;
			VK_FALSE,//VkBool32    depthBounds;
			VK_FALSE,//VkBool32    wideLines;
			VK_FALSE,//VkBool32    largePoints;
			VK_FALSE,//VkBool32    alphaToOne;
			VK_FALSE,//VkBool32    multiViewport;
			VK_FALSE,//VkBool32    samplerAnisotropy;
			VK_FALSE,//VkBool32    textureCompressionETC2;
			VK_FALSE,//VkBool32    textureCompressionASTC_LDR;
			VK_FALSE,//VkBool32    textureCompressionBC;
			VK_FALSE,//VkBool32    occlusionQueryPrecise;
			VK_FALSE,//VkBool32    pipelineStatisticsQuery;
			VK_FALSE,//VkBool32    vertexPipelineStoresAndAtomics;
			VK_FALSE,//VkBool32    fragmentStoresAndAtomics;
			VK_FALSE,//VkBool32    shaderTessellationAndGeometryPointSize;
			VK_FALSE,//VkBool32    shaderImageGatherExtended;
			VK_FALSE,//VkBool32    shaderStorageImageExtendedFormats;
			VK_FALSE,//VkBool32    shaderStorageImageMultisample;
			VK_FALSE,//VkBool32    shaderStorageImageReadWithoutFormat;
			VK_FALSE,//VkBool32    shaderStorageImageWriteWithoutFormat;
			VK_FALSE,//VkBool32    shaderUniformBufferArrayDynamicIndexing;
			VK_FALSE,//VkBool32    shaderSampledImageArrayDynamicIndexing;
			VK_FALSE,//VkBool32    shaderStorageBufferArrayDynamicIndexing;
			VK_FALSE,//VkBool32    shaderStorageImageArrayDynamicIndexing;
			VK_FALSE,//VkBool32    shaderClipDistance;
			VK_FALSE,//VkBool32    shaderCullDistance;
			VK_FALSE,//VkBool32    shaderFloat64;
			VK_FALSE,//VkBool32    shaderInt64;
			VK_FALSE,//VkBool32    shaderInt16;
			VK_FALSE,//VkBool32    shaderResourceResidency;
			VK_FALSE,//VkBool32    shaderResourceMinLod;
			VK_FALSE,//VkBool32    sparseBinding;
			VK_FALSE,//VkBool32    sparseResidencyBuffer;
			VK_FALSE,//VkBool32    sparseResidencyImage2D;
			VK_FALSE,//VkBool32    sparseResidencyImage3D;
			VK_FALSE,//VkBool32    sparseResidency2Samples;
			VK_FALSE,//VkBool32    sparseResidency4Samples;
			VK_FALSE,//VkBool32    sparseResidency8Samples;
			VK_FALSE,//VkBool32    sparseResidency16Samples;
			VK_FALSE,//VkBool32    sparseResidencyAliased;
			VK_FALSE,//VkBool32    variableMultisampleRate;
			VK_FALSE //VkBool32    inheritedQueries;		
		};
		
		static const char* loc_device_extentions [] =
		{
			VK_KHR_SWAPCHAIN_EXTENSION_NAME
		};

		auto required_layers = vkfw::Instance::default_layers ();

		VkDeviceCreateInfo loc_create_info =
		{
			VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
			nullptr,
			0u,
			1u,
			&loc_queue_info,
			(std::uint32_t)required_layers.size(),
			required_layers.data(),
			(std::uint32_t)std::size (loc_device_extentions),
			std::data (loc_device_extentions),
			&loc_features
		};

		auto loc_result = vkCreateDevice (m_vkfwPhysicalDevice.value(), &loc_create_info, allocators(), &m_vk_logical_device);
		verify_vkresult (loc_result);

		vkGetDeviceQueue (m_vk_logical_device, m_vkfwQueueIndex, 0, &m_vk_graphics_queue);
	}

	void teardown_logical_device () 
	{
		vkDestroyDevice (m_vk_logical_device, allocators ());
	}

	VkExtent2D choose_swap_extent(const VkSurfaceCapabilitiesKHR& capabilities) const
	{
	    if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) 
		{
	        return capabilities.currentExtent;
	    } 
		else 
		{
			auto x = 0;
			auto y = 0;
			glfwGetWindowSize (m_window, &x, &y);
	        VkExtent2D actualExtent = {(unsigned)x, (unsigned)y};
	
	        actualExtent.width  = std::max(capabilities.minImageExtent.width,  std::min(capabilities.maxImageExtent.width,  actualExtent.width));
	        actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));
	
	        return actualExtent;
	    }
	}

	void setup_swapchain () 
	{
		VkSurfaceCapabilitiesKHR loc_caps;
		std::vector<VkSurfaceFormatKHR> loc_formats;
		std::vector<VkPresentModeKHR> loc_modes;

		std::tie (loc_caps, loc_formats, loc_modes) = surface_capabilities (m_vkfwPhysicalDevice.value(), m_vkfwSurface.value ());

		std::sort (loc_modes.begin (), loc_modes.end ());

		if (std::binary_search (loc_modes.begin (), loc_modes.end (), VK_PRESENT_MODE_FIFO_RELAXED_KHR))
			m_vk_present_mode = VK_PRESENT_MODE_FIFO_RELAXED_KHR;
		if (std::binary_search (loc_modes.begin (), loc_modes.end (), VK_PRESENT_MODE_FIFO_KHR))
			m_vk_present_mode = VK_PRESENT_MODE_FIFO_KHR;
		if (std::binary_search (loc_modes.begin (), loc_modes.end (), VK_PRESENT_MODE_MAILBOX_KHR))
			m_vk_present_mode = VK_PRESENT_MODE_MAILBOX_KHR;
		
		bool loc_supports_needed_color_spaces = false;
		for (const auto& it : loc_formats)
		{
			if (it.format == m_vk_surface_format.format &&
				it.colorSpace == m_vk_surface_format.colorSpace)
			{
				loc_supports_needed_color_spaces = true;
			}
		}
		if (!loc_supports_needed_color_spaces)
		{
			throw std::runtime_error("Swapchain surface does not support needed format.");
		}

		m_vk_swapchain_extent = choose_swap_extent(loc_caps);
		
		VkSwapchainCreateInfoKHR loc_swapchain_info =
		{
			VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
			nullptr,
			0u,
			m_vkfwSurface.value(),
			loc_caps.minImageCount,
			m_vk_surface_format.format,
			m_vk_surface_format.colorSpace,
			m_vk_swapchain_extent,
			1u,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
			VK_SHARING_MODE_EXCLUSIVE,
			1u,			
			&m_vkfwQueueIndex,
			loc_caps.currentTransform,
			VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
			m_vk_present_mode,
			VK_TRUE,
			nullptr			
		};

		auto loc_result = vkCreateSwapchainKHR(m_vk_logical_device, &loc_swapchain_info, allocators(), &m_vk_swapchain);		
		verify_vkresult(loc_result);
		
		std::uint32_t loc_count = 0u;
		loc_result = vkGetSwapchainImagesKHR(m_vk_logical_device, m_vk_swapchain, &loc_count, nullptr);
		m_vk_swapchain_images.resize(loc_count);
		loc_result = vkGetSwapchainImagesKHR(m_vk_logical_device, m_vk_swapchain, &loc_count, m_vk_swapchain_images.data());
		verify_vkresult(loc_result);

		for (auto& loc_im : m_vk_swapchain_images)
		{
			VkImageViewCreateInfo loc_view_info =
			{
				VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
				nullptr,
				0u,
				loc_im,
				VK_IMAGE_VIEW_TYPE_2D,
				m_vk_surface_format.format,
				{	
					VK_COMPONENT_SWIZZLE_IDENTITY, 
					VK_COMPONENT_SWIZZLE_IDENTITY, 
					VK_COMPONENT_SWIZZLE_IDENTITY, 
					VK_COMPONENT_SWIZZLE_IDENTITY
				},
				{
					VK_IMAGE_ASPECT_COLOR_BIT,
					0,
					1,
					0,
					1 			
				}
			};

			m_vk_swapchain_image_views.emplace_back();
			loc_result = vkCreateImageView (m_vk_logical_device, &loc_view_info, allocators(), &m_vk_swapchain_image_views.back());
			verify_vkresult (loc_result);
		}

	}

	void teardown_swapchain ()
	{
		for (const auto& loc_imv: m_vk_swapchain_image_views)
		{
			vkDestroyImageView(m_vk_logical_device, loc_imv, allocators());
		}
		//for (const auto& loc_img: m_vk_swapchain_images)
		//{
		//	vkDestroyImage(m_vk_logical_device, loc_img, allocators());
		//}
		vkDestroySwapchainKHR(m_vk_logical_device, m_vk_swapchain, allocators());
	}

	void setup_shaders ()
	{
		auto loc_vert_shader_module = load_binary("shaders/test.vert.spv");
		auto loc_frag_shader_module = load_binary("shaders/test.frag.spv");
		VkShaderModuleCreateInfo loc_info =
		{
			VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO, 
			nullptr,
			0u,
			0u,
			nullptr
		};

		VkShaderModule loc_module;
		VkResult loc_result;

		loc_info.codeSize = loc_vert_shader_module.size();
		loc_info.pCode = (const std::uint32_t*)loc_vert_shader_module.data();
		loc_result = vkCreateShaderModule(m_vk_logical_device, &loc_info, allocators(), &loc_module);
		verify_vkresult (loc_result);
		m_vk_shader_modules.push_back (loc_module);

		loc_info.codeSize = loc_frag_shader_module.size ();
		loc_info.pCode = (const std::uint32_t*)loc_frag_shader_module.data ();
		loc_result = vkCreateShaderModule(m_vk_logical_device, &loc_info, allocators(), &loc_module);
		verify_vkresult (loc_result);
		m_vk_shader_modules.push_back (loc_module);
	}

	void teardown_shaders ()
	{
		for(const auto& it : m_vk_shader_modules)
		{
			vkDestroyShaderModule(m_vk_logical_device, it, allocators ());
		}
	}

	void setup_pipeline ()
	{
		VkPipelineShaderStageCreateInfo loc_vert_stage_info =
		{
			VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			nullptr,
			0u,
			VK_SHADER_STAGE_VERTEX_BIT,
			m_vk_shader_modules[0],
			"main",
			nullptr
		};

		VkPipelineShaderStageCreateInfo loc_frag_stage_info =
		{
			VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			nullptr,
			0u,
			VK_SHADER_STAGE_FRAGMENT_BIT,
			m_vk_shader_modules[1],
			"main",
			nullptr
		};

		VkPipelineShaderStageCreateInfo loc_shader_stages [] =
		{
			loc_vert_stage_info,
			loc_frag_stage_info
		};	

		VkPipelineVertexInputStateCreateInfo loc_vertex_inputs =
		{
			VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
			nullptr,
			0u,
			0u,
			nullptr,
			0u,
			nullptr
		};

		VkPipelineInputAssemblyStateCreateInfo loc_input_assembly =
		{
			VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
			nullptr,
			0u,
			VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
			VK_FALSE
		};

		VkViewport loc_viewport =
		{
			0.0f, 0.0f,
			(float)m_vk_swapchain_extent.width,
			(float)m_vk_swapchain_extent.height,
			0.0f, 
			1.0f
		};

		VkRect2D loc_scissor =
		{
			{0, 0},
			m_vk_swapchain_extent
		};

		VkPipelineViewportStateCreateInfo loc_viewport_state =
		{	
			VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
			nullptr,
			0u,
			1u,
			&loc_viewport,
			1u,
			&loc_scissor
		};

		VkPipelineRasterizationStateCreateInfo loc_rasterizer_info =
		{
			VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
			nullptr,
			0u,
			VK_FALSE,
			VK_FALSE,
			VK_POLYGON_MODE_FILL,
			VK_CULL_MODE_BACK_BIT,
			VK_FRONT_FACE_CLOCKWISE,
			VK_FALSE,
			0.0f,
			0.0f,
			0.0f,
			1.0f
		};

		VkPipelineMultisampleStateCreateInfo loc_multisampling_info =
		{
			VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
			nullptr,
			0u,
			VK_SAMPLE_COUNT_1_BIT,
			VK_FALSE,
			1.0f,
			nullptr,
			VK_FALSE,
			VK_FALSE
		};

		VkPipelineColorBlendAttachmentState loc_blending_attachment_state =
		{
			VK_FALSE,
			VK_BLEND_FACTOR_ONE,
			VK_BLEND_FACTOR_ZERO,
			VK_BLEND_OP_ADD,
			VK_BLEND_FACTOR_ONE,
			VK_BLEND_FACTOR_ZERO,
			VK_BLEND_OP_ADD,
			VK_COLOR_COMPONENT_R_BIT|
			VK_COLOR_COMPONENT_G_BIT|
			VK_COLOR_COMPONENT_B_BIT|
			VK_COLOR_COMPONENT_A_BIT
		};

		VkPipelineColorBlendStateCreateInfo loc_blending_state_create_info = 
		{
			VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
			nullptr,
			0u,
			VK_FALSE,
			VK_LOGIC_OP_COPY,
			1u,
			&loc_blending_attachment_state,
			{0.0f, 0.0f, 0.0f, 0.0f}
		};

		VkDynamicState loc_dynamic_states [] =
		{
			VK_DYNAMIC_STATE_VIEWPORT
		};

		VkPipelineDynamicStateCreateInfo loc_dynamic_state_create_info =
		{
			VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
			nullptr,
			0u,
			(std::uint32_t)std::size(loc_dynamic_states),
			std::data(loc_dynamic_states)
		};

		VkPipelineLayoutCreateInfo loc_pipeline_layout_info=
		{
			VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
			nullptr, 
			0u,
			0u,
			nullptr,
			0u,
			nullptr
		};

		auto loc_result = vkCreatePipelineLayout(m_vk_logical_device, &loc_pipeline_layout_info, allocators(), &m_vk_pipeline_layout);
		verify_vkresult (loc_result);

		VkAttachmentDescription loc_color_attachment = 
		{
			0u,
			m_vk_surface_format.format,
			VK_SAMPLE_COUNT_1_BIT,
			VK_ATTACHMENT_LOAD_OP_CLEAR,
			VK_ATTACHMENT_STORE_OP_STORE,
			VK_ATTACHMENT_LOAD_OP_DONT_CARE,
			VK_ATTACHMENT_STORE_OP_DONT_CARE,
			VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
		};

		VkAttachmentReference loc_attachment_ref =
		{
			0u, 
			VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		};

		VkSubpassDescription loc_subpass =
		{
			0u,
			VK_PIPELINE_BIND_POINT_GRAPHICS,
			0u,
			nullptr,
			1u,
			&loc_attachment_ref,
			nullptr,
			nullptr,
			0u,
			nullptr
		};

		VkRenderPassCreateInfo loc_renderpass_create_info= 
		{
			VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
			nullptr,
			0u,
			1u,
			&loc_color_attachment,
			1u,
			&loc_subpass,
			0u,
			nullptr
		};

		VkSubpassDependency loc_dependency = {};
		loc_dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		loc_dependency.dstSubpass = 0;
		loc_dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		loc_dependency.srcAccessMask = 0;
		loc_dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		loc_dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

		loc_renderpass_create_info.dependencyCount = 1;
		loc_renderpass_create_info.pDependencies = &loc_dependency;

		loc_result = vkCreateRenderPass(m_vk_logical_device, &loc_renderpass_create_info, allocators(), &m_vk_render_pass);
		verify_vkresult (loc_result);

		VkGraphicsPipelineCreateInfo loc_pipeline_create_info =
		{
			VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
			nullptr,
			0u,
			(std::uint32_t)std::size(loc_shader_stages),
			std::data(loc_shader_stages),
			&loc_vertex_inputs,
			&loc_input_assembly,
			nullptr,
			&loc_viewport_state,
			&loc_rasterizer_info,
			&loc_multisampling_info,
			nullptr,
			&loc_blending_state_create_info,
			&loc_dynamic_state_create_info,
			m_vk_pipeline_layout,
			m_vk_render_pass,
			0u,
			nullptr,
			-1
		};

		loc_result = vkCreateGraphicsPipelines(m_vk_logical_device, nullptr, 1u, &loc_pipeline_create_info, allocators(), &m_vk_pipeline);
		verify_vkresult (loc_result);
	}

	void teardown_pipeline()
	{
		vkDestroyPipeline(m_vk_logical_device, m_vk_pipeline, allocators ());
		vkDestroyRenderPass (m_vk_logical_device, m_vk_render_pass, allocators());
		vkDestroyPipelineLayout (m_vk_logical_device, m_vk_pipeline_layout, allocators());
	}

	void setup_framebuffers()
	{
		for(const auto& loc_imv: m_vk_swapchain_image_views)
		{
			VkFramebufferCreateInfo loc_fbci =
			{
				VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
				nullptr,
				0u,
				m_vk_render_pass,
				1u,
				&loc_imv,
				m_vk_swapchain_extent.width,
				m_vk_swapchain_extent.height,
				1u
			};
			auto loc_fbo = VkFramebuffer ();
			auto loc_result = vkCreateFramebuffer(m_vk_logical_device, &loc_fbci, allocators(), &loc_fbo);
			verify_vkresult(loc_result);
			m_vk_framebuffers.push_back(loc_fbo);
		}
	}

	void teardown_framebuffers()
	{
		for (const auto& loc_fbo : m_vk_framebuffers)
		{
			vkDestroyFramebuffer(m_vk_logical_device, loc_fbo, allocators());
		}
	}

	void setup_command_buffers()
	{
		VkCommandPoolCreateInfo loc_cpci =
		{
			VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
			nullptr,
			0u,
			m_vkfwQueueIndex
		};

		auto loc_result = vkCreateCommandPool(m_vk_logical_device, &loc_cpci, allocators(), &m_vk_command_pool);
		verify_vkresult(loc_result);

		VkCommandBufferAllocateInfo loc_cbai =
		{
			VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			nullptr,
			m_vk_command_pool,
			VK_COMMAND_BUFFER_LEVEL_PRIMARY,
			(std::uint32_t)m_vk_swapchain_images.size()
		};

		m_vk_command_buffers.resize (m_vk_swapchain_images.size());
		loc_result = vkAllocateCommandBuffers(m_vk_logical_device, &loc_cbai, m_vk_command_buffers.data ());
		verify_vkresult(loc_result);

		VkCommandBufferBeginInfo loc_cbbi =
		{
			VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
			nullptr,
			VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
			nullptr
		};

		VkClearValue loc_cv = {0.0f, 0.0f, 0.0f, 1.0f};

		VkRenderPassBeginInfo loc_rpbi =
		{
			VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
			nullptr,
			m_vk_render_pass,
			nullptr,
			{{0,0}, m_vk_swapchain_extent},
			1u,
			&loc_cv
		};

		VkViewport loc_vp =
		{
			0.0f, 0.0f,
			(float)m_vk_swapchain_extent.width,
			(float)m_vk_swapchain_extent.height,
			0.0f, 1.0f,
		};

		for (auto i = 0; i < m_vk_command_buffers.size(); ++i)
		{
			loc_result = vkBeginCommandBuffer(m_vk_command_buffers[i], &loc_cbbi);
			verify_vkresult(loc_result);
			loc_rpbi.framebuffer = m_vk_framebuffers[i];
			vkCmdSetViewport(m_vk_command_buffers[i], 0, 1u, &loc_vp);
			vkCmdBeginRenderPass(m_vk_command_buffers[i], &loc_rpbi, VK_SUBPASS_CONTENTS_INLINE);
			vkCmdBindPipeline (m_vk_command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, m_vk_pipeline);
			vkCmdDraw (m_vk_command_buffers[i], 3, 1, 0, 0);
			vkCmdEndRenderPass (m_vk_command_buffers[i]);
			loc_result = vkEndCommandBuffer(m_vk_command_buffers[i]);
			verify_vkresult(loc_result);
		}

	}

	void teardown_command_buffers()
	{
		vkDestroyCommandPool(m_vk_logical_device, m_vk_command_pool, allocators());
	}

	void setup_semaphores()
	{
		VkSemaphoreCreateInfo loc_sci =
		{
			VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
			nullptr,
			0u
		};
		VkResult loc_result ;
		loc_result = vkCreateSemaphore(m_vk_logical_device, &loc_sci, allocators(), &m_vk_sem_image_available);
		verify_vkresult(loc_result);
		loc_result = vkCreateSemaphore(m_vk_logical_device, &loc_sci, allocators(), &m_vk_sem_render_finished);
		verify_vkresult(loc_result);
	}

	void teardown_semaphores()
	{
		vkDestroySemaphore (m_vk_logical_device, m_vk_sem_image_available, allocators());		
		vkDestroySemaphore (m_vk_logical_device, m_vk_sem_render_finished, allocators());		
	}

	void draw_frame ()
	{
		uint32_t loc_image_index = 0u;
		auto loc_result = vkAcquireNextImageKHR (m_vk_logical_device, m_vk_swapchain, std::numeric_limits<uint64_t>::max(), m_vk_sem_image_available, nullptr, &loc_image_index);
		verify_vkresult(loc_result);

		VkSubmitInfo loc_submit_info = {};
		loc_submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		
		VkSemaphore loc_wait_semaphores[] = {m_vk_sem_image_available};
		VkPipelineStageFlags loc_wait_stages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
		loc_submit_info.waitSemaphoreCount = 1;
		loc_submit_info.pWaitSemaphores = loc_wait_semaphores;
		loc_submit_info.pWaitDstStageMask = loc_wait_stages;

		loc_submit_info.commandBufferCount = 1;
		loc_submit_info.pCommandBuffers = &m_vk_command_buffers[loc_image_index];

		VkSemaphore loc_signal_semaphores[] = {m_vk_sem_render_finished};
		loc_submit_info.signalSemaphoreCount = 1;
		loc_submit_info.pSignalSemaphores = loc_signal_semaphores;

		loc_result = vkQueueSubmit(m_vk_graphics_queue, 1u, &loc_submit_info, nullptr);
		verify_vkresult(loc_result);

		VkPresentInfoKHR loc_present_info = {};
		VkSwapchainKHR loc_swap_chains[] = {m_vk_swapchain};

		loc_present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		loc_present_info.waitSemaphoreCount = 1;
		loc_present_info.pWaitSemaphores = loc_signal_semaphores;
		loc_present_info.pResults = nullptr; // Optional
		loc_present_info.swapchainCount = 1u;
		loc_present_info.pSwapchains = &loc_swap_chains[0];
		loc_present_info.pImageIndices = &loc_image_index;

		vkQueuePresentKHR(m_vk_graphics_queue, &loc_present_info);
	}


	void init()
	{
		setup_window ();
		m_vkfwInstance = vkfw::Instance::create ();		
		m_vkfwDebugReport = vkfw::DebugReport::create(std::cout, m_vkfwInstance.value ());		
		m_vkfwSurface = vkfw::Surface::create ({m_window, m_vkfwInstance.value ()});

		vkfw::DeviceLocator loc_locator;
		vkfw::DeviceLocator::device_and_queue_index_collection loc_result;
		vkfw::DeviceLocator::query loc_query;
		loc_query.device_type_required (loc_query.Find_any_gpu);
		loc_query.features_required (vkfw::Feature_geometry_shader|vkfw::Feature_tessellation_shader);
		loc_query.queues_required ().push_back 
		(
			vkfw::QueueLocator::query ()
				.must_support_surface(m_vkfwSurface)
				.queue_capability_required(vkfw::QueueLocator::query ().Queue_grahics)
				.queue_ammount_required(1u)
		);

		if (!loc_locator.find(loc_result, m_vkfwInstance.devices(), loc_query))
		{
			throw std::runtime_error ("Unable to find suitable device");
		}
		m_vkfwPhysicalDevice = std::move(loc_result.front().device);
		m_vkfwQueueIndex = loc_result.front().queues.front();


		setup_logical_device ();
		setup_semaphores();
		setup_swapchain ();
		setup_shaders();
		setup_pipeline();
		setup_framebuffers();
		setup_command_buffers();
	}

	void quit()
	{
		teardown_command_buffers();
		teardown_framebuffers();
		teardown_pipeline();
		teardown_shaders();
		teardown_swapchain ();
		teardown_semaphores();
		teardown_logical_device ();

		m_vkfwSurface.clear ();
		m_vkfwDebugReport.clear();
		m_vkfwInstance.clear ();

		teardown_window ();
	}


	void loop ()
	{
	    while (!glfwWindowShouldClose(m_window)) 
		{
	        glfwPollEvents();
			draw_frame ();
	    }	
		vkDeviceWaitIdle(m_vk_logical_device);
	}

	vkfw::Instance						m_vkfwInstance;
	vkfw::DebugReport					m_vkfwDebugReport;
	vkfw::PhysicalDevice				m_vkfwPhysicalDevice;
	std::uint32_t						m_vkfwQueueIndex;
	vkfw::Surface						m_vkfwSurface ;

	
	VkSwapchainKHR						m_vk_swapchain						= nullptr;
	std::vector<VkImage>				m_vk_swapchain_images;
	std::vector<VkImageView>			m_vk_swapchain_image_views;
	VkPresentModeKHR					m_vk_present_mode					= VK_PRESENT_MODE_IMMEDIATE_KHR;
	VkSurfaceFormatKHR					m_vk_surface_format					= {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
	VkExtent2D							m_vk_swapchain_extent;

	VkDevice							m_vk_logical_device					= nullptr;
	VkQueue								m_vk_graphics_queue					= nullptr;

	VkCommandPool						m_vk_command_pool					= nullptr;
	std::vector<VkCommandBuffer>		m_vk_command_buffers;
	std::vector<VkFramebuffer>			m_vk_framebuffers;

	std::vector<VkShaderModule>			m_vk_shader_modules;	
	VkPipelineLayout					m_vk_pipeline_layout				= nullptr;
	VkRenderPass						m_vk_render_pass					= nullptr;
	VkPipeline							m_vk_pipeline						= nullptr;

	VkSemaphore							m_vk_sem_image_available			= nullptr;
	VkSemaphore							m_vk_sem_render_finished			= nullptr;

	GLFWwindow*							m_window							= nullptr;
};

int main() 
{
	try
	{	
		
		GlideApp loc_app;

		loc_app.init ();
		loc_app.loop ();
		loc_app.quit ();

	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what () << "\n";
		__debugbreak();
	}
	return 0;
}