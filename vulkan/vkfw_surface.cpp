#include "vkfw_surface.hpp"
#include "vkfw_exception.hpp"

vkfw::Surface vkfw::Surface::create (const vkfw::Surface::glfw_arguments& loc_args) 
{
	VkSurfaceKHR loc_object = nullptr;
	auto loc_result = vkfw::maybe_throw (glfwCreateWindowSurface (loc_args.instance (), loc_args.window(), loc_args.allocators(), &loc_object));
	return vkfw::Surface(loc_object, loc_args.allocators (), loc_args.instance ());
}
