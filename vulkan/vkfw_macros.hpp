#pragma once
#include <stdexcept>

#define VKFW_DEFINE_OBJECT_TRAITS(Handle, Destroy, ...)\
	template <>\
	struct handle_destructor<Handle>\
	{\
		template <typename _Handle_container>\
		static auto destroy(const _Handle_container& loc_handle)\
		{\
			const auto context	  = loc_handle.context();\
			const auto handle	  = loc_handle.value();\
			const auto allocators = loc_handle.allocators();\
			return Destroy(__VA_ARGS__);\
		}\
	}

#define VKFW_DEFINE_PROPERTY(Type, Name, DefaultValue)\
	private:\
		Type m_##Name = DefaultValue;\
	public:\
		auto&& Name() const noexcept { return m_##Name ; }\
		auto&& Name() noexcept { return m_##Name ; }\
		template <typename _Value_type>\
		auto& Name(_Value_type&& loc_value)\
		{ m_##Name = std::forward<_Value_type>(loc_value); return *this; }

#define VKFW_DEFINE_PROPERTY_OPTIONAL_REF(Type, Name, DefaultValue)\
	private:\
		Type* m_##Name = DefaultValue;\
	public:\
		bool has_##Name () const noexcept { return m_##Name != nullptr; }\
		auto&& Name () const { if (has_##Name ()) return *m_##Name; throw std::out_of_range("Optional value is undefined."); }\
		auto&& Name ()       { if (has_##Name ()) return *m_##Name; throw std::out_of_range("Optional value is undefined."); }\
		auto& Name(Type& loc_value) noexcept { m_##Name = &loc_value; return *this; }\
		auto& Name(nullptr_t) noexcept { m_##Name = nullptr; return *this; }
