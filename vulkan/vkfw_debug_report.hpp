#pragma once

#include <functional>
#include <memory>
#include <string>
#include <sstream>

#include <vulkan/vulkan.h>

#include "vkfw_handle.hpp"
#include "vkfw_instance.hpp"

extern "C"
{
	VkResult vkCreateDebugReportCallbackEXT	(VkInstance, const VkDebugReportCallbackCreateInfoEXT*, const VkAllocationCallbacks*, VkDebugReportCallbackEXT*);
	void vkDestroyDebugReportCallbackEXT (VkInstance, VkDebugReportCallbackEXT, const VkAllocationCallbacks*);
}

namespace vkfw
{	
	std::string to_string (VkDebugReportFlagsEXT flags);
	std::string to_string (VkDebugReportObjectTypeEXT object_type);

	namespace detail
	{
		VKFW_DEFINE_OBJECT_TRAITS(VkDebugReportCallbackEXT, ::vkDestroyDebugReportCallbackEXT, (VkInstance)context, handle, allocators);
	}

	struct DebugReport: handle<VkDebugReportCallbackEXT>
	{
		using handle<VkDebugReportCallbackEXT>::handle;

		enum format_specifier_type: std::uint32_t
		{
			Format_json		= 0,
			Format_xml		= 1,
			Format_text		= 2,
			Format_brief	= 3
		};
		
		static constexpr auto Information	= VK_DEBUG_REPORT_INFORMATION_BIT_EXT;
		static constexpr auto Warning		= VK_DEBUG_REPORT_WARNING_BIT_EXT;
		static constexpr auto Performance	= VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
		static constexpr auto Error			= VK_DEBUG_REPORT_ERROR_BIT_EXT;
		static constexpr auto Debug			= VK_DEBUG_REPORT_DEBUG_BIT_EXT;
		static constexpr auto All			= Information|Debug|Error|Performance|Warning;		

		struct arguments
		{
			arguments (VkInstance loc_instance = nullptr, const VkAllocationCallbacks* loc_allocators = nullptr):
				m_allocators	(loc_allocators),
				m_instance		(loc_instance)
			{}

			VKFW_DEFINE_PROPERTY(const VkAllocationCallbacks*, allocators, nullptr);
			VKFW_DEFINE_PROPERTY(VkInstance, instance, nullptr);
			VKFW_DEFINE_PROPERTY(VkDebugReportFlagsEXT, flags, vkfw::DebugReport::All);
			VKFW_DEFINE_PROPERTY(format_specifier_type, format, Format_brief);
		};

		struct log_item_type
		{
			VkDebugReportFlagsEXT       flags;
			VkDebugReportObjectTypeEXT  object_type;
			uint64_t                    object;
			size_t                      location;
			int32_t                     message_code;
			std::string                 layer_prefix;
			std::string                 message;
		};
		
		static DebugReport create(std::ostream& loc_stream, const arguments& loc_args = arguments ());		
	
	private:

		struct format_dispatch_entry
		{
			void (*prolog)  (std::ostream&);
			void (*epilog)  (std::ostream&);
			void (*message) (std::ostream&, const log_item_type&);
		};
		
		static void format_prolog_json   (std::ostream&);
		static void format_epilog_json   (std::ostream&);
		static void format_message_json  (std::ostream&, const log_item_type& loc_item);
		static void format_prolog_xml    (std::ostream&);
		static void format_epilog_xml    (std::ostream&);
		static void format_message_xml   (std::ostream&, const log_item_type& loc_item);
		static void format_prolog_text   (std::ostream&);
		static void format_epilog_text   (std::ostream&);
		static void format_message_text  (std::ostream&, const log_item_type& loc_item);
		static void format_prolog_brief  (std::ostream&);
		static void format_epilog_brief  (std::ostream&);
		static void format_message_brief (std::ostream&, const log_item_type& loc_item);

		struct i_log
		{
			virtual VkBool32 write(const log_item_type& loc_item) = 0;
			virtual ~i_log() = default;
		};

		template <typename _Stream, std::size_t _Format_index>
		struct t_stream_log final: i_log 
		{
			t_stream_log (_Stream& loc_stream): 
				m_stream (loc_stream) 
			{ 
				m_format_dispatch_table[_Format_index].prolog (m_stream);
			}

			~t_stream_log () override final
			{ 
				m_format_dispatch_table[_Format_index].epilog (m_stream);
			}

			VkBool32 write(const log_item_type& loc_item) override final
			{
				m_format_dispatch_table[_Format_index].message (m_stream, loc_item);
				return VK_FALSE;
			}
		private:
			_Stream& m_stream;
		};

		std::unique_ptr<i_log>			m_logger_ptr = nullptr;
		static format_dispatch_entry	m_format_dispatch_table [4];

	};

	
}

