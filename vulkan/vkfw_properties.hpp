#pragma once

#include <cstdint>
#include <string>

#include <vulkan/vulkan.h>

namespace vkfw
{
	struct ExtensionProperties:
		VkExtensionProperties
	{
		std::uint32_t specification_version () const noexcept { return VkExtensionProperties::specVersion; }
		std::string name () const { return VkExtensionProperties::extensionName; }

		friend bool operator <  (const ExtensionProperties& a, const ExtensionProperties& b) { return a.name () <  b.name (); }
		friend bool operator >  (const ExtensionProperties& a, const ExtensionProperties& b) { return a.name () >  b.name (); }
		friend bool operator <= (const ExtensionProperties& a, const ExtensionProperties& b) { return a.name () <= b.name (); }
		friend bool operator >= (const ExtensionProperties& a, const ExtensionProperties& b) { return a.name () >= b.name (); }
		friend bool operator == (const ExtensionProperties& a, const ExtensionProperties& b) { return a.name () == b.name (); }
		friend bool operator != (const ExtensionProperties& a, const ExtensionProperties& b) { return a.name () != b.name (); }

		friend bool operator <  (const ExtensionProperties& a, const std::string& b) { return a.name () <  b; }
		friend bool operator >  (const ExtensionProperties& a, const std::string& b) { return a.name () >  b; }
		friend bool operator <= (const ExtensionProperties& a, const std::string& b) { return a.name () <= b; }
		friend bool operator >= (const ExtensionProperties& a, const std::string& b) { return a.name () >= b; }
		friend bool operator == (const ExtensionProperties& a, const std::string& b) { return a.name () == b; }
		friend bool operator != (const ExtensionProperties& a, const std::string& b) { return a.name () != b; }

		friend bool operator <  (const std::string& a, const ExtensionProperties& b) { return a <  b.name (); }
		friend bool operator >  (const std::string& a, const ExtensionProperties& b) { return a >  b.name (); }
		friend bool operator <= (const std::string& a, const ExtensionProperties& b) { return a <= b.name (); }
		friend bool operator >= (const std::string& a, const ExtensionProperties& b) { return a >= b.name (); }
		friend bool operator == (const std::string& a, const ExtensionProperties& b) { return a == b.name (); }
		friend bool operator != (const std::string& a, const ExtensionProperties& b) { return a != b.name (); }

		operator std::string () const { return name(); }
	};

	struct LayerProperties:
		VkLayerProperties
	{
		std::uint32_t specification_version () const noexcept { return VkLayerProperties::specVersion; }
		std::uint32_t implementation_version () const noexcept { return VkLayerProperties::implementationVersion; }
		std::string name () const { return VkLayerProperties::layerName; }
		std::string description () const { return VkLayerProperties::description; }		

		friend bool operator <  (const LayerProperties& a, const LayerProperties& b) { return a.name () <  b.name (); }
		friend bool operator >  (const LayerProperties& a, const LayerProperties& b) { return a.name () >  b.name (); }
		friend bool operator <= (const LayerProperties& a, const LayerProperties& b) { return a.name () <= b.name (); }
		friend bool operator >= (const LayerProperties& a, const LayerProperties& b) { return a.name () >= b.name (); }
		friend bool operator == (const LayerProperties& a, const LayerProperties& b) { return a.name () == b.name (); }
		friend bool operator != (const LayerProperties& a, const LayerProperties& b) { return a.name () != b.name (); }

		friend bool operator <  (const LayerProperties& a, const std::string& b) { return a.name () <  b; }
		friend bool operator >  (const LayerProperties& a, const std::string& b) { return a.name () >  b; }
		friend bool operator <= (const LayerProperties& a, const std::string& b) { return a.name () <= b; }
		friend bool operator >= (const LayerProperties& a, const std::string& b) { return a.name () >= b; }
		friend bool operator == (const LayerProperties& a, const std::string& b) { return a.name () == b; }
		friend bool operator != (const LayerProperties& a, const std::string& b) { return a.name () != b; }

		friend bool operator <  (const std::string& a, const LayerProperties& b) { return a <  b.name (); }
		friend bool operator >  (const std::string& a, const LayerProperties& b) { return a >  b.name (); }
		friend bool operator <= (const std::string& a, const LayerProperties& b) { return a <= b.name (); }
		friend bool operator >= (const std::string& a, const LayerProperties& b) { return a >= b.name (); }
		friend bool operator == (const std::string& a, const LayerProperties& b) { return a == b.name (); }
		friend bool operator != (const std::string& a, const LayerProperties& b) { return a != b.name (); }

		operator std::string () const { return name(); }
	};

}
