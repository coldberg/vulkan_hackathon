#pragma once

#include <vulkan/vulkan.h>

#include <vector>

#include "vkfw_macros.hpp"
#include "vkfw_surface.hpp"
#include "vkfw_physical.hpp"

namespace vkfw
{
	struct QueueLocator
	{

		struct query
		{
			static constexpr auto Queue_grahics				= VK_QUEUE_GRAPHICS_BIT;
			static constexpr auto Queue_compute				= VK_QUEUE_COMPUTE_BIT;
			static constexpr auto Queue_transfer			= VK_QUEUE_TRANSFER_BIT;
			static constexpr auto Qeueu_sparse_bindings		= VK_QUEUE_SPARSE_BINDING_BIT;

			VKFW_DEFINE_PROPERTY				(std::uint32_t, queue_capability_required,	Queue_grahics);
			VKFW_DEFINE_PROPERTY				(std::uint32_t, queue_ammount_required,		1u);
			VKFW_DEFINE_PROPERTY_OPTIONAL_REF	(const Surface, must_support_surface,		nullptr);
		};

		bool find(std::vector<std::uint32_t>& loc_indexes, const PhysicalDevice& loc_device, const query& loc_query) const;

	private:
		bool get_devices(std::vector<VkQueueFamilyProperties>&, const PhysicalDevice& loc_device) const;
	};
}