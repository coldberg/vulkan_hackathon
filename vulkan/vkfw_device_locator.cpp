#include "vkfw_device_locator.hpp"

static bool is_correct_device_type(VkPhysicalDeviceType loc_type, std::uint32_t loc_type_required)
{
	switch (loc_type)
	{
	case VK_PHYSICAL_DEVICE_TYPE_CPU:
		return !!(loc_type_required & vkfw::DeviceLocator::query::Find_cpu);				
	case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
		return !!(loc_type_required & vkfw::DeviceLocator::query::Find_discrete_gpu);
	case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
		return !!(loc_type_required & vkfw::DeviceLocator::query::Find_integrated_gpu);
	case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
		return !!(loc_type_required & vkfw::DeviceLocator::query::Find_virtual_gpu);
	case VK_PHYSICAL_DEVICE_TYPE_OTHER:
		return !!(loc_type_required & vkfw::DeviceLocator::query::Find_other);
	default:
		break;
	}
	return false;
}

bool vkfw::DeviceLocator::find(device_and_queue_index_collection& loc_result, const std::vector<PhysicalDevice>& loc_physical_devices, const query & loc_query)
{
	vkfw::QueueLocator loc_queue_locator;
	const auto loc_prev_result = loc_result.size();

	for (const auto& loc_device : loc_physical_devices)
	{
		if (!is_correct_device_type(loc_device.properties().deviceType, loc_query.device_type_required())
			|| (loc_device.features() & loc_query.features_required()) != loc_query.features_required())
		{
			continue;
		}
		
		std::vector<std::uint32_t> loc_indexes;
		for (const auto& loc_queue_query : loc_query.queues_required())
		{
			std::vector<std::uint32_t> loc_temp_indexes;
			if (!loc_queue_locator.find(loc_temp_indexes, loc_device, loc_queue_query))
			{
				loc_indexes.clear();
				break;
			}
			loc_indexes.push_back (loc_temp_indexes.front ());
		}

		if (loc_indexes.size() < 1u)
		{
			continue;
		}

		loc_result.emplace_back(loc_device, loc_indexes);
	}
	return loc_result.size() > loc_prev_result;
}
