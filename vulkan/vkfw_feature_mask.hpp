#pragma once

#include <cstdint>
#include <cstddef>

#include <vulkan/vulkan.h>

namespace vkfw
{
	static constexpr auto Feature_index_robust_buffer_access							= 0;
	static constexpr auto Feature_index_full_draw_index_uint32							= 1;
	static constexpr auto Feature_index_image_cube_array								= 2;
	static constexpr auto Feature_index_independent_blend								= 3;
	static constexpr auto Feature_index_geometry_shader									= 4;
	static constexpr auto Feature_index_tessellation_shader								= 5;
	static constexpr auto Feature_index_sample_rate_shading								= 6;
	static constexpr auto Feature_index_dual_src_blend									= 7;
	static constexpr auto Feature_index_logic_op										= 8;
	static constexpr auto Feature_index_multi_draw_indirect								= 9;
	static constexpr auto Feature_index_draw_indirect_first_instance					= 10;
	static constexpr auto Feature_index_depth_clamp										= 11;
	static constexpr auto Feature_index_depth_bias_clamp								= 12;
	static constexpr auto Feature_index_fill_mode_non_solid								= 13;
	static constexpr auto Feature_index_depth_bounds									= 14;
	static constexpr auto Feature_index_wide_lines										= 15;
	static constexpr auto Feature_index_large_points									= 16;
	static constexpr auto Feature_index_alpha_to_one									= 17;
	static constexpr auto Feature_index_multi_viewport									= 18;
	static constexpr auto Feature_index_sampler_anisotropy								= 19;
	static constexpr auto Feature_index_texture_compression_ETC2						= 20;
	static constexpr auto Feature_index_texture_compression_ASTC_LDR					= 21;
	static constexpr auto Feature_index_texture_compression_BC							= 22;
	static constexpr auto Feature_index_occlusion_query_precise							= 23;
	static constexpr auto Feature_index_pipeline_statistics_query						= 24;
	static constexpr auto Feature_index_vertex_pipeline_stores_and_atomics				= 25;
	static constexpr auto Feature_index_fragment_stores_and_atomics						= 26;
	static constexpr auto Feature_index_shader_tessellation_and_geometry_point_size		= 27;
	static constexpr auto Feature_index_shader_image_gather_extended					= 28;
	static constexpr auto Feature_index_shader_storage_image_extended_formats			= 29;
	static constexpr auto Feature_index_shader_storage_image_multisample				= 30;
	static constexpr auto Feature_index_shader_storage_image_read_without_format		= 31;
	static constexpr auto Feature_index_shader_storage_image_write_without_format		= 32;
	static constexpr auto Feature_index_shader_uniform_buffer_array_dynamic_indexing	= 33;
	static constexpr auto Feature_index_shader_sampled_image_array_dynamic_indexing		= 34;
	static constexpr auto Feature_index_shader_storage_buffer_array_dynamic_indexing	= 35;
	static constexpr auto Feature_index_shader_storage_image_array_dynamic_indexing		= 36;
	static constexpr auto Feature_index_shader_clip_distance							= 37;
	static constexpr auto Feature_index_shader_cull_distance							= 38;
	static constexpr auto Feature_index_shader_float64									= 39;
	static constexpr auto Feature_index_shader_int64									= 40;
	static constexpr auto Feature_index_shader_int16									= 41;
	static constexpr auto Feature_index_shader_resource_residency						= 42;
	static constexpr auto Feature_index_shader_resource_min_lod							= 43;
	static constexpr auto Feature_index_sparse_binding									= 44;
	static constexpr auto Feature_index_sparse_residency_buffer							= 45;
	static constexpr auto Feature_index_sparse_residency_image2d						= 46;
	static constexpr auto Feature_index_sparse_residency_image3d						= 47;
	static constexpr auto Feature_index_sparse_residency2_samples						= 48;
	static constexpr auto Feature_index_sparse_residency4_samples						= 49;
	static constexpr auto Feature_index_sparse_residency8_samples						= 50;
	static constexpr auto Feature_index_sparse_residency16_samples						= 51;
	static constexpr auto Feature_index_sparse_residency_aliased						= 52;
	static constexpr auto Feature_index_variable_multisample_rate						= 53;
	static constexpr auto Feature_index_inherited_queries								= 54;

	static constexpr auto Feature_none											    = 0ull;
	static constexpr auto Feature_robust_buffer_access								= 1ull << 0ull;
	static constexpr auto Feature_full_draw_index_uint32							= 1ull << 1ull;
	static constexpr auto Feature_image_cube_array									= 1ull << 2ull;
	static constexpr auto Feature_independent_blend									= 1ull << 3ull;
	static constexpr auto Feature_geometry_shader									= 1ull << 4ull;
	static constexpr auto Feature_tessellation_shader								= 1ull << 5ull;
	static constexpr auto Feature_sample_rate_shading								= 1ull << 6ull;
	static constexpr auto Feature_dual_src_blend									= 1ull << 7ull;
	static constexpr auto Feature_logic_op											= 1ull << 8ull;
	static constexpr auto Feature_multi_draw_indirect								= 1ull << 9ull;
	static constexpr auto Feature_draw_indirect_first_instance						= 1ull << 10ull;
	static constexpr auto Feature_depth_clamp										= 1ull << 11ull;
	static constexpr auto Feature_depth_bias_clamp									= 1ull << 12ull;
	static constexpr auto Feature_fill_mode_non_solid								= 1ull << 13ull;
	static constexpr auto Feature_depth_bounds										= 1ull << 14ull;
	static constexpr auto Feature_wide_lines										= 1ull << 15ull;
	static constexpr auto Feature_large_points										= 1ull << 16ull;
	static constexpr auto Feature_alpha_to_one										= 1ull << 17ull;
	static constexpr auto Feature_multi_viewport									= 1ull << 18ull;
	static constexpr auto Feature_sampler_anisotropy								= 1ull << 19ull;
	static constexpr auto Feature_texture_compression_ETC2							= 1ull << 20ull;
	static constexpr auto Feature_texture_compression_ASTC_LDR						= 1ull << 21ull;
	static constexpr auto Feature_texture_compression_BC							= 1ull << 22ull;
	static constexpr auto Feature_occlusion_query_precise							= 1ull << 23ull;
	static constexpr auto Feature_pipeline_statistics_query							= 1ull << 24ull;
	static constexpr auto Feature_vertex_pipeline_stores_and_atomics				= 1ull << 25ull;
	static constexpr auto Feature_fragment_stores_and_atomics						= 1ull << 26ull;
	static constexpr auto Feature_shader_tessellation_and_geometry_point_size		= 1ull << 27ull;
	static constexpr auto Feature_shader_image_gather_extended						= 1ull << 28ull;
	static constexpr auto Feature_shader_storage_image_extended_formats				= 1ull << 29ull;
	static constexpr auto Feature_shader_storage_image_multisample					= 1ull << 30ull;
	static constexpr auto Feature_shader_storage_image_read_without_format			= 1ull << 31ull;
	static constexpr auto Feature_shader_storage_image_write_without_format			= 1ull << 32ull;
	static constexpr auto Feature_shader_uniform_buffer_array_dynamic_indexing		= 1ull << 33ull;
	static constexpr auto Feature_shader_sampled_image_array_dynamic_indexing		= 1ull << 34ull;
	static constexpr auto Feature_shader_storage_buffer_array_dynamic_indexing		= 1ull << 35ull;
	static constexpr auto Feature_shader_storage_image_array_dynamic_indexing		= 1ull << 36ull;
	static constexpr auto Feature_shader_clip_distance								= 1ull << 37ull;
	static constexpr auto Feature_shader_cull_distance								= 1ull << 38ull;
	static constexpr auto Feature_shader_float64									= 1ull << 39ull;
	static constexpr auto Feature_shader_int64										= 1ull << 40ull;
	static constexpr auto Feature_shader_int16										= 1ull << 41ull;
	static constexpr auto Feature_shader_resource_residency							= 1ull << 42ull;
	static constexpr auto Feature_shader_resource_min_lod							= 1ull << 43ull;
	static constexpr auto Feature_sparse_binding									= 1ull << 44ull;
	static constexpr auto Feature_sparse_residency_buffer							= 1ull << 45ull;
	static constexpr auto Feature_sparse_residency_image2d							= 1ull << 46ull;
	static constexpr auto Feature_sparse_residency_image3d							= 1ull << 47ull;
	static constexpr auto Feature_sparse_residency2_samples							= 1ull << 48ull;
	static constexpr auto Feature_sparse_residency4_samples							= 1ull << 49ull;
	static constexpr auto Feature_sparse_residency8_samples							= 1ull << 50ull;
	static constexpr auto Feature_sparse_residency16_samples						= 1ull << 51ull;
	static constexpr auto Feature_sparse_residency_aliased							= 1ull << 52ull;
	static constexpr auto Feature_variable_multisample_rate							= 1ull << 53ull;
	static constexpr auto Feature_inherited_queries									= 1ull << 54ull;


	typedef std::uint64_t PhysicalDeviceFeatureMask;
	
	PhysicalDeviceFeatureMask physical_device_feature_mask_from_features(const VkPhysicalDeviceFeatures& loc_features);
	void physical_device_features_from_feature_mask (VkPhysicalDeviceFeatures& loc_features, const vkfw::PhysicalDeviceFeatureMask& loc_mask);

}

