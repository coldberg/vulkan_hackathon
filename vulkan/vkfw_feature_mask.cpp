#include "vkfw_feature_mask.hpp"

vkfw::PhysicalDeviceFeatureMask vkfw::physical_device_feature_mask_from_features(const VkPhysicalDeviceFeatures& loc_features)
{
	auto loc_begin = (const VkBool32*)&loc_features;
	auto loc_end = (const VkBool32*)((&loc_features) + 1u);

	vkfw::PhysicalDeviceFeatureMask loc_mask = 0ull;

	for (const auto* i = loc_begin; i != loc_end; ++i)
	{
		if (*i)
		{
			loc_mask |= (1ull << (i - loc_begin));
		}
	}

	return loc_mask;
}

void vkfw::physical_device_features_from_feature_mask (VkPhysicalDeviceFeatures& loc_features, const vkfw::PhysicalDeviceFeatureMask& loc_mask)
{
	auto loc_begin = (VkBool32*)&loc_features;
	auto loc_end = (VkBool32*)((&loc_features) + 1u);

	for (auto* i = loc_begin; i != loc_end; ++i)
	{
		*i = !!(loc_mask & (1ull << (i - loc_begin)));
	}
}