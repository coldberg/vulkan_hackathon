#pragma once

#include <stdexcept>

#include <vulkan/vulkan.h>

namespace vkfw
{
	struct exception: std::exception
	{
		exception (VkResult loc_code) noexcept;

		virtual const char* what () const noexcept override;
		const VkResult& value() const noexcept
		{
			return m_code;
		}
	private:
		VkResult m_code;
	};

	bool to_throw_or_not_to_throw(VkResult loc_result) noexcept;
	VkResult maybe_throw (VkResult loc_result);
}