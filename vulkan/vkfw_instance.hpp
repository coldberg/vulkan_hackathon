#pragma once

#include <vector>
#include <string>

#include "vkfw_handle.hpp"
#include "vkfw_properties.hpp"
#include "vkfw_array_proxy.hpp"
#include "vkfw_macros.hpp"
#include "vkfw_physical.hpp"
#include "vkfw_feature_mask.hpp"
#include "vkfw_surface.hpp"
#include "vkfw_device_locator.hpp"

namespace vkfw
{
	struct Instance: handle<VkInstance>
	{	
		using handle<VkInstance>::handle;
		
		typedef std::vector<const char*> vector_of_literals_type;
		typedef std::vector<PhysicalDevice> vector_of_physical_devices_type;

		struct arguments
		{
			VKFW_DEFINE_PROPERTY(std::uint32_t,						application_version,	VK_MAKE_VERSION(0, 0, 1));
			VKFW_DEFINE_PROPERTY(std::string,						application_name,		"VkfwApplication");
			VKFW_DEFINE_PROPERTY(std::uint32_t,						engine_version,			VK_MAKE_VERSION(0, 0, 1));
			VKFW_DEFINE_PROPERTY(std::string,						engine_name,			"VulkanFramework");
			VKFW_DEFINE_PROPERTY(std::uint32_t,						api_version,			VK_API_VERSION_1_0);
			VKFW_DEFINE_PROPERTY(vkfw::array_proxy<const char*>,	extensions,				Instance::default_extensions());
			VKFW_DEFINE_PROPERTY(vkfw::array_proxy<const char*>,	layers,					Instance::default_layers());
			VKFW_DEFINE_PROPERTY(const VkAllocationCallbacks*,		allocators,				nullptr);
		};

		static Instance create (const arguments& = arguments());

		static auto extensions (const std::string& loc_layer) -> std::vector<ExtensionProperties>;
		static auto extensions () -> std::vector<ExtensionProperties>;
		static auto layers () -> std::vector<LayerProperties>;

		static bool has_extenstion (const std::string& loc_name, const std::string& loc_layer);
		static bool has_extenstion (const std::string& loc_name);
		static bool has_layer (const std::string& loc_name);
		static void require_extensions (const std::vector<const char*>& loc_names, const std::string& loc_layer) { return require_extensions (std::vector<std::string>(loc_names.begin(), loc_names.end()), loc_layer); }
		static void require_extensions (const std::vector<const char*>& loc_names) { return require_extensions (std::vector<std::string>(loc_names.begin(), loc_names.end())); }
		static void require_layers     (const std::vector<const char*>& loc_names) { return require_layers (std::vector<std::string>(loc_names.begin(), loc_names.end())); }
		static void require_extensions (const std::vector<std::string>& loc_names, const std::string& loc_layer);
		static void require_extensions (const std::vector<std::string>& loc_names);
		static void require_layers     (const std::vector<std::string>& loc_names);

		auto devices () const noexcept -> const vector_of_physical_devices_type&;

		static auto default_extensions () -> const vector_of_literals_type&;
		static auto default_layers	   () -> const vector_of_literals_type&;
		static auto default_allocators () -> const VkAllocationCallbacks*;	
		
		DeviceLocator device_locator () const noexcept;

	private:
		vector_of_physical_devices_type m_physical_devices;
	};

}
