#pragma once

#include <vector>
#include <string>
#include <memory>
#include <array>

namespace vkfw
{
	template <typename _Value_type>
	struct array_proxy
	{
		typedef _Value_type value_type;
		array_proxy():
			m_data(nullptr),
			m_size(0u)
		{}

		array_proxy(const std::vector<value_type>& loc_source): 
			m_data(std::data(loc_source)),
			m_size(std::size(loc_source))
		{}

		array_proxy(const std::basic_string<value_type>& loc_source):
			m_data(std::data(loc_source)),
			m_size(std::size(loc_source))
		{}

		array_proxy(const std::initializer_list<value_type>& loc_source):
			m_data(std::data(loc_source)),
			m_size(std::size(loc_source))
		{}

		template <std::size_t _N_count>
		array_proxy(const std::array<value_type, _N_count>& loc_source):
			m_data(std::data(loc_source)),
			m_size(std::size(loc_source))
		{}

		template <std::size_t _N_count>
		array_proxy(const value_type (&loc_source) [_N_count]):
			m_data(std::data(loc_source)),
			m_size(std::size(loc_source))
		{}
		
		const value_type* data () const noexcept
		{
			return m_data;
		}

		std::size_t size () const noexcept
		{
			return m_size;
		}
		std::uint32_t size32 () const noexcept
		{
			return (std::uint32_t)m_size;
		}

		template <typename _New_value_type = _Value_type>
		std::vector<_New_value_type> as_vector_of() const 
		{
			return std::vector<_New_value_type>(data(), data() + size());
		}

	private:
		const value_type* m_data;
		std::size_t m_size;
	};

}
