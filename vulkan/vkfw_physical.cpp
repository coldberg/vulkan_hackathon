#include <algorithm>

#include "vkfw_physical.hpp"
#include "vkfw_exception.hpp"


static auto feature_mask_from_device(VkPhysicalDevice loc_device)
{
	if (loc_device == nullptr)
	{
		return vkfw::PhysicalDeviceFeatureMask{};
	}
	VkPhysicalDeviceFeatures loc_features = {};
	vkGetPhysicalDeviceFeatures(loc_device, &loc_features);
	return vkfw::physical_device_feature_mask_from_features(loc_features);
}

static auto physical_device_properties(VkPhysicalDevice loc_device)
{
	VkPhysicalDeviceProperties loc_properties = {};
	if (loc_device != nullptr)
	{
		vkGetPhysicalDeviceProperties(loc_device, &loc_properties);
	}
	return loc_properties;
}

vkfw::PhysicalDevice::PhysicalDevice (VkPhysicalDevice loc_device):
	m_device	(loc_device),
	m_features	(feature_mask_from_device (loc_device)),
	m_props		(physical_device_properties (loc_device))
{

}

vkfw::PhysicalDevice::PhysicalDevice (const PhysicalDevice& loc_copy):
	m_device	(loc_copy.m_device),
	m_features	(loc_copy.m_features),
	m_props		(loc_copy.m_props)
{	
}

vkfw::PhysicalDevice::PhysicalDevice (PhysicalDevice&& loc_move):
	m_device	(std::exchange (loc_move.m_device,   nullptr)),
	m_features	(std::exchange (loc_move.m_features, vkfw::PhysicalDeviceFeatureMask {})),
	m_props		(std::exchange (loc_move.m_props,	 VkPhysicalDeviceProperties {}))
{
}

vkfw::PhysicalDevice& vkfw::PhysicalDevice::operator = (const PhysicalDevice& loc_copy) 
{
	this->~PhysicalDevice ();
	new (this) PhysicalDevice (loc_copy);
	return *this;
}

vkfw::PhysicalDevice& vkfw::PhysicalDevice::operator = (PhysicalDevice&& loc_move) 
{
	this->~PhysicalDevice ();
	new (this) PhysicalDevice (std::move(loc_move));
	return *this;	
}


auto vkfw::PhysicalDevice::extensions () const -> std::vector<vkfw::ExtensionProperties> 
{
	std::uint32_t loc_count = 0u;	
	VkResult loc_result = VK_SUCCESS;
	loc_result = vkfw::maybe_throw (vkEnumerateDeviceExtensionProperties (m_device, nullptr, &loc_count, nullptr));	
	std::vector<vkfw::ExtensionProperties> loc_props(loc_count);
	loc_result = vkfw::maybe_throw (vkEnumerateDeviceExtensionProperties (m_device, nullptr, &loc_count, loc_props.data()));		
	std::sort(loc_props.begin(), loc_props.end());
	return loc_props;
}

auto vkfw::PhysicalDevice::extensions (const std::string& loc_layer) const -> std::vector<vkfw::ExtensionProperties>
{
	std::uint32_t loc_count = 0u;	
	VkResult loc_result = VK_SUCCESS;
	loc_result = vkfw::maybe_throw (vkEnumerateDeviceExtensionProperties (m_device, loc_layer.c_str() , &loc_count, nullptr));	
	 std::vector<vkfw::ExtensionProperties> loc_props(loc_count);
	loc_result = vkfw::maybe_throw (vkEnumerateDeviceExtensionProperties (m_device, loc_layer.c_str() , &loc_count, loc_props.data()));		
	std::sort(loc_props.begin(), loc_props.end());
	return loc_props;
}

auto vkfw::PhysicalDevice::properties () const -> const VkPhysicalDeviceProperties&
{
	return m_props;
}

auto vkfw::PhysicalDevice::features () const -> const vkfw::PhysicalDeviceFeatureMask&
{
	return m_features;
}

bool vkfw::PhysicalDevice::can_present_to (const vkfw::Surface& loc_surface, std::uint32_t loc_queue_index) const 
{
	VkBool32 loc_result = VK_FALSE;
	auto loc_success = vkfw::maybe_throw (vkGetPhysicalDeviceSurfaceSupportKHR(m_device, loc_queue_index, loc_surface.value(), &loc_result));
	return loc_result == VK_TRUE;
}

bool vkfw::PhysicalDevice::is_gpu () const noexcept 
{
	switch (properties().deviceType)
	{
	case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: 
	case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
	case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
		return true;
	default:
		return false;			
	}	
	return false;
}

bool vkfw::PhysicalDevice::is_cpu () const noexcept 
{
	return properties().deviceType == VK_PHYSICAL_DEVICE_TYPE_CPU;
}

std::string vkfw::PhysicalDevice::name () const 
{
	return properties().deviceName;
}
