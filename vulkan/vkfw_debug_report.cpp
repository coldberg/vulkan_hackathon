#include <vector>

#include "vkfw_debug_report.hpp"
#include "vkfw_proc_addr.hpp"
#include "vkfw_util.hpp"
#include "vkfw_exception.hpp"

VkResult vkCreateDebugReportCallbackEXT (VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback) 
{
	VKFW_DEFINE_INSTANCE_PROC (instance, vkCreateDebugReportCallbackEXT);
	return vkCreateDebugReportCallbackEXT (instance, pCreateInfo, pAllocator, pCallback);
}

void vkDestroyDebugReportCallbackEXT (VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* pAllocator) 
{
	VKFW_DEFINE_INSTANCE_PROC (instance, vkDestroyDebugReportCallbackEXT);
	return vkDestroyDebugReportCallbackEXT (instance, callback, pAllocator);
}

std::string vkfw::to_string (VkDebugReportFlagsEXT flags) 
{
	using namespace std::string_literals;
	std::vector<std::string> loc_system;

#define _CASE(X) if (flags & X) loc_system.push_back(#X);	
	_CASE (VK_DEBUG_REPORT_INFORMATION_BIT_EXT);
	_CASE (VK_DEBUG_REPORT_WARNING_BIT_EXT);
	_CASE (VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT);
	_CASE (VK_DEBUG_REPORT_ERROR_BIT_EXT);
	_CASE (VK_DEBUG_REPORT_DEBUG_BIT_EXT);
#undef _CASE

	return vkfw::util::join (loc_system, "|"s);
}

std::string vkfw::to_string (VkDebugReportObjectTypeEXT object_type) 
{
#define _CASE(X) case X: return #X
	switch (object_type) {
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_KHR_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_MODE_KHR_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_OBJECT_TABLE_NVX_EXT);
		_CASE (VK_DEBUG_REPORT_OBJECT_TYPE_INDIRECT_COMMANDS_LAYOUT_NVX_EXT);
	default:
		return "";
	}
#undef _CASE

}


vkfw::DebugReport vkfw::DebugReport::create (std::ostream& loc_stream, const arguments& loc_args) 
{
	std::unique_ptr<i_log> loc_writter_ptr = nullptr;	
	switch (loc_args.format())
	{
	case Format_json:
		loc_writter_ptr = std::make_unique<t_stream_log<std::ostream, Format_json>> (loc_stream);
		break;
	case Format_xml:
		loc_writter_ptr = std::make_unique<t_stream_log<std::ostream, Format_xml>> (loc_stream);
		break;
	case Format_text:
		loc_writter_ptr = std::make_unique<t_stream_log<std::ostream, Format_text>> (loc_stream);
		break;
	case Format_brief:
	default:
		loc_writter_ptr = std::make_unique<t_stream_log<std::ostream, Format_brief>> (loc_stream);
		break;
	}

	VkDebugReportCallbackCreateInfoEXT loc_info =
	{
		VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT,
		nullptr, 
		loc_args.flags(),
		[] (VkDebugReportFlagsEXT       flags,
			VkDebugReportObjectTypeEXT  objectType,
			uint64_t                    object,
			size_t                      location,
			int32_t                     messageCode,
			const char*                 pLayerPrefix,
			const char*                 pMessage,
			void*                       pUserData) 
			-> VkBool32
		{
			auto* writter = reinterpret_cast<i_log*> (pUserData);

			if (!writter)
			{
				return VK_FALSE;
			}

			return writter->write (vkfw::DebugReport::log_item_type 
			{
				flags,
				objectType,
				object,
				location,
				messageCode,
				pLayerPrefix,
				pMessage			
			});
		},
		loc_writter_ptr.get ()
	};

	VkDebugReportCallbackEXT loc_object = nullptr;
	auto loc_result = vkfw::maybe_throw (vkCreateDebugReportCallbackEXT(loc_args.instance(), &loc_info, loc_args.allocators(), &loc_object));	
	auto loc_report = vkfw::DebugReport (loc_object, loc_args.allocators(), loc_args.instance ());
	loc_report.m_logger_ptr = std::move(loc_writter_ptr);
	return loc_report;
}

void vkfw::DebugReport::format_prolog_json (std::ostream& loc_ostream)
{
	loc_ostream << "[\n";
}

void vkfw::DebugReport::format_epilog_json (std::ostream& loc_ostream) {
	loc_ostream << "{}]\n";
}

void vkfw::DebugReport::format_message_json (std::ostream& loc_ostream, const log_item_type& loc_item) {	
	static const auto escape_json =
		[] (const std::string& in) -> std::string
	{
		std::string output;
		for (const auto& it : in)
		{
			output.push_back(it);
			if (it == '\\')
			{
				output.push_back(it);
			}
		}
		return output;
	};

	loc_ostream
		<<  "{ \"flags\": \""		 << to_string (loc_item.flags)			<< "\", "
		<<  "  \"object_type\": \""  << to_string (loc_item.object_type)	<< "\", "
		<<  "  \"object\": \"0x"	 << std::hex << loc_item.object			<< "\", "
		<<  "  \"location\": \""	 << std::dec << loc_item.location		<< "\", "
		<<  "  \"message_code\": \"" << std::dec << loc_item.message_code	<< "\", "
		<<	"  \"layer_prefix\": \"" << loc_item.layer_prefix				<< "\", "
		<<	"  \"message\": \""		 << escape_json(loc_item.message)		<< "\"},\n";	
}

void vkfw::DebugReport::format_prolog_xml (std::ostream& loc_ostream) {
	loc_ostream << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
				<< "<log>\n";
}

void vkfw::DebugReport::format_epilog_xml (std::ostream& loc_ostream) {
	loc_ostream << "</log>\n";
}

void vkfw::DebugReport::format_message_xml (std::ostream& loc_ostream, const log_item_type& loc_item) {	
	loc_ostream
		<<  "  <item>\n"
		<<  "    <flags>"			<< to_string (loc_item.flags)			<< 	"</flags>\n"
		<<  "    <object_type>"		<< to_string (loc_item.object_type)		<< 	"</object_type>\n"
		<<  "    <object>0x"		<< std::hex << loc_item.object			<< 	"</object>\n"
		<<  "    <location>"		<< std::dec << loc_item.location		<< 	"</location>\n"
		<<  "    <message_code>"	<< std::dec << loc_item.message_code	<< 	"</message_code>\n"
		<<	"    <layer_prefix>"	<< loc_item.layer_prefix				<< 	"</layer_prefix>\n"
		<<	"    <message>"			<< loc_item.message						<< 	"</message>\n"
		<<  "  </item>\n";	
}

void vkfw::DebugReport::format_prolog_text (std::ostream& loc_ostream) 
{

}

void vkfw::DebugReport::format_epilog_text (std::ostream& loc_ostream) 
{

}

void vkfw::DebugReport::format_message_text (std::ostream& loc_ostream, const log_item_type& loc_item) 
{
	loc_ostream
		<< "       flags : "  << to_string (loc_item.flags)			<< "\n"
		<< "     message : "  << loc_item.message					<< "\n"
		<< "message_code : "  << std::dec << loc_item.message_code	<< "\n"
		<< "layer_prefix : "  << loc_item.layer_prefix				<< "\n"
		<< " object_type : "  << to_string (loc_item.object_type)	<< "\n"
		<< "      object : 0x"<< std::hex << loc_item.object		<< "\n"
		<< "    location : "  << std::dec << loc_item.location		<< "\n"
		<< "\n";
}

void vkfw::DebugReport::format_prolog_brief (std::ostream&) 
{

}

void vkfw::DebugReport::format_epilog_brief (std::ostream&) 
{

}

void vkfw::DebugReport::format_message_brief (std::ostream& loc_ostream, const log_item_type& loc_item) 
{
	loc_ostream << loc_item.layer_prefix << " : " << loc_item.message << "\n";
}

vkfw::DebugReport::format_dispatch_entry 
	vkfw::DebugReport::m_format_dispatch_table [4] =
{
	{&vkfw::DebugReport::format_prolog_json,  &vkfw::DebugReport::format_epilog_json,  &vkfw::DebugReport::format_message_json},
	{&vkfw::DebugReport::format_prolog_xml,	  &vkfw::DebugReport::format_epilog_xml,   &vkfw::DebugReport::format_message_xml},
	{&vkfw::DebugReport::format_prolog_text,  &vkfw::DebugReport::format_epilog_text,  &vkfw::DebugReport::format_message_text},
	{&vkfw::DebugReport::format_prolog_brief, &vkfw::DebugReport::format_epilog_brief, &vkfw::DebugReport::format_message_brief}
};