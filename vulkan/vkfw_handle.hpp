#pragma once

#include <tuple>
#include <type_traits>
#include <functional>

#include <vulkan/vulkan.h>

#include "vkfw_macros.hpp"

namespace vkfw 
{
	namespace detail
	{
		template <typename _Value_type>
		struct handle_destructor
		{
		
		};

		VKFW_DEFINE_OBJECT_TRAITS(VkInstance,	vkDestroyInstance,   handle, allocators);
		VKFW_DEFINE_OBJECT_TRAITS(VkDevice,		vkDestroyDevice,     handle, allocators);	
		VKFW_DEFINE_OBJECT_TRAITS(VkSurfaceKHR, vkDestroySurfaceKHR, (VkInstance)context, handle, allocators);
	}	

	template <typename _Handle_type>
	struct handle
	{
		typedef _Handle_type value_type;

		template <typename _Context_type = nullptr_t>
		handle(const value_type& loc_value, const VkAllocationCallbacks* loc_allocators = nullptr, _Context_type* loc_arguments = nullptr):
			m_value		 (loc_value),
			m_allocators (loc_allocators),
			m_context	 (loc_arguments)		
		{}

		handle(handle&& loc_prev):
			m_value		 (std::exchange(loc_prev.m_value,		nullptr)),
			m_allocators (std::exchange(loc_prev.m_allocators,	nullptr)),
			m_context    (std::exchange(loc_prev.m_context,		nullptr))
		{}

		handle():
			m_value		 (nullptr),
			m_allocators (nullptr),
			m_context	 (nullptr)		
		{}

		handle(const handle&) = delete;
		handle& operator = (const handle&) = delete;
		
		~handle()
		{
			if (m_value)
			{				
				detail::handle_destructor<value_type>::destroy(*this);
				m_value	= nullptr;
			}
			m_allocators = nullptr;
			m_context = nullptr;
		}

		handle& operator = (handle&& loc_prev)
		{
			this->~handle();
			new(this) handle<value_type>(std::move(loc_prev));
			return *this;
		}

		auto&& value () noexcept { return m_value; }
		auto&& value () const noexcept { return m_value; }

		void clear()
		{
			this->~handle();			
		}

		operator const value_type& () const noexcept
		{
			return m_value;
		}

		auto&& allocators() const noexcept { return m_allocators; }

		template <typename _Ttype = void*>
		auto&& context() const noexcept { return (_Ttype)m_context; }

	private:
		value_type m_value;
		const VkAllocationCallbacks* m_allocators;
		void* m_context;
	};

}
