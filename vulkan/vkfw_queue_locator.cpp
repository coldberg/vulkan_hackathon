#include "vkfw_queue_locator.hpp"

bool vkfw::QueueLocator::find(std::vector<std::uint32_t>& loc_indexes, const PhysicalDevice& loc_device, const query& loc_query) const
{
	std::vector<VkQueueFamilyProperties> loc_queues;
	get_devices(loc_queues, loc_device);
	if (loc_queues.empty())
	{
		return false;
	}

	const auto loc_queue_caps = loc_query.queue_capability_required();
	const auto loc_prev_size = loc_indexes.size();

	std::uint32_t loc_index = 0u;

	for (const auto& i_queue : loc_queues)
	{			
		if ((i_queue.queueFlags & loc_queue_caps) == loc_queue_caps 
		  && i_queue.queueCount >= loc_query.queue_ammount_required())
		{
			if (!loc_query.has_must_support_surface())
			{
				loc_indexes.push_back(loc_index);
			}
			else
			{
				if (loc_device.can_present_to(loc_query.must_support_surface(), loc_index))
				{
					loc_indexes.push_back(loc_index);
				}
			}			
		}
		++loc_index;		
	}
	return loc_indexes.size() > loc_prev_size;
}

bool vkfw::QueueLocator::get_devices(std::vector<VkQueueFamilyProperties>& loc_queues, const PhysicalDevice& loc_device) const
{
	std::uint32_t loc_count = 0u;
	vkGetPhysicalDeviceQueueFamilyProperties(loc_device.value(), &loc_count, nullptr);
	loc_queues.resize(loc_count);
	vkGetPhysicalDeviceQueueFamilyProperties(loc_device.value(), &loc_count, loc_queues.data());
	return loc_count > 0u;
}
