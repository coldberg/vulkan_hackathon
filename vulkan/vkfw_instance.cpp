#include <algorithm>
#include <string>

#include "vkfw_instance.hpp"
#include "vkfw_exception.hpp"
#include "vkfw_util.hpp"

vkfw::Instance vkfw::Instance::create (const vkfw::Instance::arguments& loc_args)
{
	Instance::require_extensions (loc_args.extensions ().as_vector_of<std::string>());
	Instance::require_layers	 (loc_args.layers	  ().as_vector_of<std::string>());

	VkApplicationInfo App_info = 
	{
		VK_STRUCTURE_TYPE_APPLICATION_INFO, nullptr, 
		"VkfwFramework",
		0x1,
		"VulkanFramework",
		0x1,
		loc_args.api_version()
	};

	VkInstanceCreateInfo Inst_info =
	{
		VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO, nullptr, 0u,
		&App_info,
		loc_args.layers     ().size32(),
		loc_args.layers     ().data(),
		loc_args.extensions ().size32(),
		loc_args.extensions ().data()
	};

	VkInstance loc_instance = nullptr;
	auto loc_vkerr = vkfw::maybe_throw (vkCreateInstance (&Inst_info, loc_args.allocators(), &loc_instance));	

	auto loc_instance_object = vkfw::Instance (loc_instance, loc_args.allocators());

	std::uint32_t loc_count = 0u;
	VkResult loc_result = VK_SUCCESS;
	loc_result = vkfw::maybe_throw (vkEnumeratePhysicalDevices (loc_instance, &loc_count, nullptr));
	std::vector<VkPhysicalDevice> loc_devices (loc_count);		
	loc_result = vkfw::maybe_throw (vkEnumeratePhysicalDevices (loc_instance, &loc_count, loc_devices.data ()));

	loc_instance_object.m_physical_devices.assign (loc_devices.begin(), loc_devices.end());

	return loc_instance_object;
}

auto vkfw::Instance::extensions (const std::string& loc_layer)
	-> std::vector<vkfw::ExtensionProperties> 
{
	std::uint32_t loc_size = 0u;
	auto loc_vkerr = vkEnumerateInstanceExtensionProperties (loc_layer.c_str (), &loc_size, nullptr);
	if (vkfw::to_throw_or_not_to_throw (loc_vkerr)) 
	{
		throw vkfw::exception (loc_vkerr);
	}
	std::vector<ExtensionProperties> loc_store (loc_size);
	loc_vkerr = vkEnumerateInstanceExtensionProperties (loc_layer.c_str (), &loc_size, loc_store.data ());
	if (vkfw::to_throw_or_not_to_throw (loc_vkerr)) 
	{
		throw vkfw::exception (loc_vkerr);
	}
	std::sort(std::begin(loc_store), std::end(loc_store));
	return loc_store;
}

auto vkfw::Instance::extensions () 
	-> std::vector<ExtensionProperties> 
{
	std::uint32_t loc_size = 0u;
	auto loc_vkerr = vkEnumerateInstanceExtensionProperties (nullptr, &loc_size, nullptr);
	if (vkfw::to_throw_or_not_to_throw (loc_vkerr)) 
	{
		throw vkfw::exception (loc_vkerr);
	}
	std::vector<ExtensionProperties> loc_store (loc_size);
	loc_vkerr = vkEnumerateInstanceExtensionProperties (nullptr, &loc_size, loc_store.data ());
	if (vkfw::to_throw_or_not_to_throw (loc_vkerr)) 
	{
		throw vkfw::exception (loc_vkerr);
	}
	std::sort(std::begin(loc_store), std::end(loc_store));
	return loc_store;
}

auto vkfw::Instance::layers () 
	-> std::vector<LayerProperties> 
{
	std::uint32_t loc_size = 0u;
	auto loc_vkerr = vkEnumerateInstanceLayerProperties (&loc_size, nullptr);
	if (vkfw::to_throw_or_not_to_throw (loc_vkerr)) 
	{
		throw vkfw::exception (loc_vkerr);
	}
	std::vector<LayerProperties> loc_store (loc_size);
	loc_vkerr = vkEnumerateInstanceLayerProperties (&loc_size, loc_store.data ());
	if (vkfw::to_throw_or_not_to_throw (loc_vkerr)) 
	{
		throw vkfw::exception (loc_vkerr);
	}
	std::sort(std::begin(loc_store), std::end(loc_store));
	return loc_store;
}

bool vkfw::Instance::has_extenstion (const std::string& loc_needle, const std::string& loc_layer) 
{
	return vkfw::util::contains_one(Instance::extensions(loc_layer), loc_needle);
}

bool vkfw::Instance::has_extenstion (const std::string& loc_needle) 
{
	return vkfw::util::contains_one(Instance::extensions(), loc_needle);
}

bool vkfw::Instance::has_layer (const std::string& loc_needle) 
{
	return vkfw::util::contains_one(Instance::layers(), loc_needle);
}

void vkfw::Instance::require_extensions (const std::vector<std::string>& loc_names, const std::string& loc_layer) 
{
	using namespace std::string_literals;
	auto loc_haystack = vkfw::Instance::extensions(loc_layer);
	if (!vkfw::util::remove_intersecting (loc_haystack, loc_names, true))
	{
		auto loc_missing = std::vector<std::string> (loc_haystack.begin (), loc_haystack.end ());
		throw std::runtime_error ("Layer ("s + loc_layer + ") does not support rquired extensions :\n\t"s + vkfw::util::join (loc_missing, "\n\t"s) + "\n"s);
	}
}

void vkfw::Instance::require_extensions (const std::vector<std::string>& loc_names) 
{
	using namespace std::string_literals;
	auto loc_haystack = vkfw::Instance::extensions();
	if (!vkfw::util::remove_intersecting (loc_haystack, loc_names, true))
	{
		auto loc_missing = std::vector<std::string> (loc_haystack.begin (), loc_haystack.end ());
		throw std::runtime_error ("Instance does not support rquired extensions :\n\t"s + vkfw::util::join (loc_missing, "\n\t"s) + "\n"s);
	}
}

void vkfw::Instance::require_layers (const std::vector<std::string>& loc_names) 
{
	using namespace std::string_literals;
	auto loc_haystack = vkfw::Instance::layers();
	if (!vkfw::util::remove_intersecting (loc_haystack, loc_names, true))
	{
		auto loc_missing = std::vector<std::string> (loc_haystack.begin (), loc_haystack.end ());
		throw std::runtime_error ("Instance does not support rquired layers :\n\t"s + vkfw::util::join (loc_missing, "\n\t"s) + "\n"s);
	}
}

auto vkfw::Instance::devices () const noexcept -> const vector_of_physical_devices_type&
{	
	return m_physical_devices;
}

auto vkfw::Instance::default_extensions () -> const vector_of_literals_type&
{
	static const auto st_data = vector_of_literals_type
	{			
		"VK_KHR_surface",
		"VK_KHR_win32_surface",
	#ifdef _DEBUG
		"VK_EXT_debug_report",
	#endif		
	};
	return st_data;
}

auto vkfw::Instance::default_layers () -> const vector_of_literals_type&
{
	static const auto st_data = vector_of_literals_type
	{	
#ifdef _DEBUG			
		"VK_LAYER_GOOGLE_threading",
		"VK_LAYER_LUNARG_parameter_validation",
		"VK_LAYER_LUNARG_object_tracker",
		"VK_LAYER_LUNARG_core_validation",
		"VK_LAYER_LUNARG_image",
		"VK_LAYER_LUNARG_swapchain",
		"VK_LAYER_GOOGLE_unique_objects",			
		"VK_LAYER_NV_nsight"
#endif							
	};
	return st_data;
}

auto vkfw::Instance::default_allocators () -> const VkAllocationCallbacks*
{
	return nullptr;
}



