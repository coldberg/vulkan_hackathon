#pragma once

#include "vkfw_macros.hpp"
#include "vkfw_feature_mask.hpp"
#include "vkfw_physical.hpp"
#include "vkfw_queue_locator.hpp"

namespace vkfw
{

	struct DeviceLocator
	{				


		struct query
		{
			static constexpr auto Find_discrete_gpu		= 0x0001;
			static constexpr auto Find_integrated_gpu	= 0x0002;
			static constexpr auto Find_virtual_gpu		= 0x0004;
			static constexpr auto Find_cpu				= 0x0008;
			static constexpr auto Find_other			= 0x0010;
			static constexpr auto Find_physical_gpu		= Find_discrete_gpu|Find_integrated_gpu;
			static constexpr auto Find_any_gpu			= Find_discrete_gpu|Find_integrated_gpu|Find_virtual_gpu;
			static constexpr auto Find_any				= Find_any_gpu|Find_cpu|Find_other;

			VKFW_DEFINE_PROPERTY(std::uint32_t,						device_type_required,	Find_any);
			VKFW_DEFINE_PROPERTY(PhysicalDeviceFeatureMask,			features_required,		Feature_none);
			VKFW_DEFINE_PROPERTY(std::vector<QueueLocator::query>,	queues_required,		{ QueueLocator::query() });
		};

		struct device_and_queue_indexes
		{
			device_and_queue_indexes(PhysicalDevice loc_device, std::vector<std::uint32_t> loc_queues = {}) :
				device (std::move (loc_device)),
				queues (loc_queues)
			{}

			PhysicalDevice device;
			std::vector<std::uint32_t> queues;
		};

		typedef std::vector<device_and_queue_indexes> device_and_queue_index_collection;

		bool find(device_and_queue_index_collection& loc_devices, const std::vector<PhysicalDevice>& loc_physical_devices, const query& loc_query = query());
	};
	
}
