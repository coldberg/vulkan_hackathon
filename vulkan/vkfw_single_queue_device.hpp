#pragma once

#include <vector>

#include <vulkan/vulkan.h>

#include "vkfw_macros.hpp"
#include "vkfw_handle.hpp"
#include "vkfw_physical.hpp"

namespace vkfw
{
	struct SingleQueueDevice: handle<VkDevice>
	{
		using handle<VkDevice>::handle;	

		struct arguments
		{
			VKFW_DEFINE_PROPERTY(std::uint32_t, queue_family_index, 0u);
			VKFW_DEFINE_PROPERTY_OPTIONAL_REF(const PhysicalDevice, physical_device, nullptr);
		};

		static SingleQueueDevice create(const arguments&);
	};
}
