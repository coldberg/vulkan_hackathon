#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <vulkan/vulkan.h>

#include "vkfw_handle.hpp"
#include "vkfw_macros.hpp"

namespace vkfw
{
	struct Surface: handle<VkSurfaceKHR>
	{
		using handle<VkSurfaceKHR>::handle;

		struct glfw_arguments
		{
			glfw_arguments (GLFWwindow* loc_window = nullptr, VkInstance loc_instance = nullptr, const VkAllocationCallbacks* loc_allocators = nullptr)
			:	m_allocators (loc_allocators),
				m_window	 (loc_window),
				m_instance	 (loc_instance)
			{}

			VKFW_DEFINE_PROPERTY (const VkAllocationCallbacks*,	allocators, nullptr);			 
			VKFW_DEFINE_PROPERTY (GLFWwindow*,					window,		nullptr);
			VKFW_DEFINE_PROPERTY (VkInstance,					instance,	nullptr);
		};

		static Surface create (const glfw_arguments& loc_args);
	private:

	};
}
