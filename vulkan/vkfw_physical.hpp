#pragma once

#include <vector>
#include <vulkan/vulkan.h>

#include "vkfw_surface.hpp"
#include "vkfw_properties.hpp"
#include "vkfw_feature_mask.hpp"


namespace vkfw
{
	struct PhysicalDevice
	{

		PhysicalDevice (VkPhysicalDevice loc_device = nullptr);
		PhysicalDevice (const PhysicalDevice&);
		PhysicalDevice (PhysicalDevice&&);
		PhysicalDevice& operator = (const PhysicalDevice&);
		PhysicalDevice& operator = (PhysicalDevice&&);

		auto extensions () const -> std::vector<ExtensionProperties>;
		auto extensions (const std::string& loc_layer) const -> std::vector<ExtensionProperties>;
		
		auto properties () const -> const VkPhysicalDeviceProperties&;
		auto features () const -> const vkfw::PhysicalDeviceFeatureMask&;

		bool can_present_to (const vkfw::Surface& loc_surface, std::uint32_t loc_queue_index) const;

		bool is_gpu () const noexcept;
		bool is_cpu () const noexcept;	

		std::string name () const;

		auto&& value () const noexcept { return m_device; }
		auto&& value () noexcept { return m_device; }

	private:
		VkPhysicalDevice m_device;
		vkfw::PhysicalDeviceFeatureMask m_features;
		VkPhysicalDeviceProperties m_props;
	};
}
