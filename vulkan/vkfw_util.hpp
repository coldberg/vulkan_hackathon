#pragma once

#include <vector>
#include <string>
#include <iterator>
#include <algorithm>

namespace vkfw
{
	namespace util
	{
		namespace detail
		{
			template <typename _String, typename _Iter>
			inline const auto& join (_String& loc_out, _Iter loc_begin, _Iter loc_end, const _String& loc_sep = _String ())
			{
				if (std::distance (loc_begin, loc_end) < 1u)
					return loc_out;
				loc_out = *loc_begin;
				if (std::distance (loc_begin, loc_end) < 2u)
					return loc_out;
				std::for_each(std::next(loc_begin), loc_end, [&] (const auto& loc_it)
				{
					loc_out.append (loc_sep);
					loc_out.append (loc_it);
				});
				return loc_out;
			}

			template <typename _String, typename _Vector>
			inline const auto& join (_String& loc_out, const _Vector& loc_list, const _String& loc_sep = _String())
			{			
				return join (loc_out, std::begin(loc_list), std::end(loc_list), loc_sep);
			}
		}
		template <typename _String, typename _Vector>
		inline auto join (const _Vector& loc_list, const _String& loc_sep = _String())
		{
			_String loc_out;
			return detail::join (loc_out, std::begin(loc_list), std::end(loc_list), loc_sep);
		}


		template <typename _Haystack_type, typename _Needle_type>
		bool contains_one (const std::vector<_Haystack_type>& loc_haystack, const _Needle_type& loc_needle, bool is_sorted = false)
		{
			if (is_sorted)
			{
				return std::binary_search (loc_haystack.begin (), loc_haystack.end (), loc_needle);	
			}
			return std::find (loc_haystack.begin (), loc_haystack.end (), loc_needle) != loc_haystack.end ();
		}

		template <typename _Haystack_type, typename _Needle_type>
		bool contains_all (const std::vector<_Haystack_type>& loc_haystack, const std::vector<_Needle_type>& loc_needle, bool is_sorted = false)
		{
			for (const auto& value : loc_needle)
			{
				if (!contains_one (loc_haystack, value, is_sorted))
				{
					return false;
				}
			}
			return true;
		}

		template <typename _Haystack_type, typename _Needle_type>
		bool remove_intersecting (std::vector<_Haystack_type>& loc_haystack, const std::vector<_Needle_type>& loc_needle, bool is_sorted = false)
		{
			std::vector<_Haystack_type> _loc_haystack;
			_loc_haystack.reserve (loc_haystack.size ());
	
			std::copy_if (loc_haystack.begin (), loc_haystack.end (), std::back_inserter(_loc_haystack), [&] (const auto& it)
			{
				return !contains_one (loc_haystack, it, true);
			});
			
			loc_haystack = _loc_haystack;
			return _loc_haystack.empty();
		}

	}
}

