#pragma once

namespace vkfw
{
#define VKFW_DEFINE_INSTANCE_PROC(Instance, Name)	PFN_##Name Name = (PFN_##Name)vkGetInstanceProcAddr(Instance, #Name)
#define VKFW_DEFINE_DEVICE_PROC(Device, Name)		PFN_##Name Name = (PFN_##Name)vkGetDeviceProcAddr(Device, #Name)
#define VKFW_GET_INSTANCE_PROC(Instance, Name)		((PFN_##Name)vkGetInstanceProcAddr(Instance, #Name))
#define VKFW_GET_DEVICE_PROC(Device, Name)			((PFN_##Name)vkGetDeviceProcAddr(Device, #Name))
}
